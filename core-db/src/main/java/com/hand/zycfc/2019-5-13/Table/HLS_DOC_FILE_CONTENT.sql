-- Create table
create table HLS_DOC_FILE_CONTENT
(
  CONTENT_ID         NUMBER not null,
  DOCUMENT_ID        NUMBER not null,
  DOCUMENT_TABLE     VARCHAR2(50),
  CLAUSE_USAGE       VARCHAR2(30),
  TEMPLET_ID         NUMBER,
  CONTENT_NUMBER     VARCHAR2(300),
  CONTENT_PRINT_FLAG VARCHAR2(2),
  AVAILABLE_FLAG     VARCHAR2(2),
  CREATION_DATE      DATE not null,
  CREATED_BY         NUMBER not null,
  LAST_UPDATE_DATE   DATE not null,
  LAST_UPDATED_BY    NUMBER not null,
  REF_V01            VARCHAR2(2000),
  REF_V02            VARCHAR2(2000),
  REF_V03            VARCHAR2(2000),
  REF_V04            VARCHAR2(2000),
  REF_V05            VARCHAR2(2000),
  REF_N01            NUMBER,
  REF_N02            NUMBER,
  REF_N03            NUMBER,
  REF_N04            NUMBER,
  REF_N05            NUMBER,
  REF_D01            DATE,
  REF_D02            DATE,
  REF_D03            DATE,
  REF_D04            DATE,
  REF_D05            DATE
);
-- Add comments to the columns
comment on column HLS_DOC_FILE_CONTENT.CLAUSE_USAGE
  is '条款用途';
comment on column HLS_DOC_FILE_CONTENT.TEMPLET_ID
  is '模版ID';
-- Create/Recreate primary, unique and foreign key constraints
alter table HLS_DOC_FILE_CONTENT
  add constraint HLS_DOC_FILE_CONTENT_PK primary key (CONTENT_ID)
  using index ;
-- Create/Recreate indexes
create index HLS_DOC_FILE_CONTENT_U1 on HLS_DOC_FILE_CONTENT (DOCUMENT_ID, DOCUMENT_TABLE, TEMPLET_ID);
