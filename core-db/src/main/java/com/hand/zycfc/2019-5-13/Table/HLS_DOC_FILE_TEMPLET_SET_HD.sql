-- Create table
create table HLS_DOC_FILE_TEMPLET_SET_HD
(
  TEMPLET_SET_ID   NUMBER not null,
  TEMPLET_SET_CODE VARCHAR2(100) not null,
  TEMPLET_SET_NAME VARCHAR2(2000) not null,
  USAGE_CODE       VARCHAR2(100),
  NOTE             VARCHAR2(2000),
  ENABLED_FLAG     VARCHAR2(1),
  CREATION_DATE    DATE not null,
  CREATED_BY       NUMBER not null,
  LAST_UPDATE_DATE DATE not null,
  LAST_UPDATED_BY  NUMBER not null
);
-- Add comments to the columns
comment on column HLS_DOC_FILE_TEMPLET_SET_HD.TEMPLET_SET_CODE
  is '模板集代码 ';
comment on column HLS_DOC_FILE_TEMPLET_SET_HD.TEMPLET_SET_NAME
  is '模板集名称';
comment on column HLS_DOC_FILE_TEMPLET_SET_HD.USAGE_CODE
  is '用途 syscode: HLS_DOC_FILE_USAGE: / CONTRACT_FILE/PROJECT_REPORT ';
comment on column HLS_DOC_FILE_TEMPLET_SET_HD.NOTE
  is '说明';
-- Create/Recreate primary, unique and foreign key constraints
alter table HLS_DOC_FILE_TEMPLET_SET_HD
  add constraint HLS_DOC_FILE_TEMPLET_SET_HD_PK primary key (TEMPLET_SET_ID)
  using index;
-- Create/Recreate indexes
create unique index HLS_DOC_FILE_TEMPLET_SET_HD_U1 on HLS_DOC_FILE_TEMPLET_SET_HD (TEMPLET_SET_CODE);
