create or replace package cus_prj_content_pkg is
  /*create by xuls for lsh project content*/

  --生成合同文本本
  PROCEDURE prj_content_create(p_project_id   NUMBER,
                               p_quotation_id NUMBER,
                               p_user_id      NUMBER);

  --打印合同文本
  PROCEDURE prj_content_print(p_project_id   NUMBER,
                              p_quotation_id NUMBER,
                              p_content_id   NUMBER,
                              p_file_path    VARCHAR2,
                              p_file_name    VARCHAR2,
                              p_type         VARCHAR2,
                              p_user_id      NUMBER);

  --删除文本
  PROCEDURE delete_prj_content(p_content_id prj_project_content.content_id%TYPE,
                               p_user_id    prj_project_content.created_by%TYPE);

  FUNCTION prj_tmpt_check(p_project_id  NUMBER,
                          p_tmpt_id     NUMBER,
                          p_user_id     NUMBER,
                          p_bp_id       NUMBER,
                          p_bp_class    VARCHAR2,
                          p_bp_category VARCHAR2) RETURN VARCHAR2;
END cus_prj_content_pkg;
/
create or replace package body cus_prj_content_pkg is
  /*create by xuls for lsh project content 2014-11-14*/

  g_check_yes CONSTANT VARCHAR2(1) := 'Y';
  g_check_no  CONSTANT VARCHAR2(1) := 'N';

  e_lock_error EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_lock_error, -54);

  /*项目状态校验*/
  PROCEDURE prj_project_check(p_project_id  NUMBER,
                              p_user_id     NUMBER,
                              p_project_rec OUT prj_project%ROWTYPE) IS
    e_prj_status_error EXCEPTION;
  BEGIN
    SELECT pp.*
      INTO p_project_rec
      FROM prj_project pp
     WHERE pp.project_id = p_project_id;
    --for update nowait;

    /*if p_project_rec.project_status in
       ('NEW', 'SIGN', 'INCEPT', 'PAID', 'APPROVING') then
      raise e_con_status_error;
    end if;*/
  EXCEPTION
    WHEN e_prj_status_error THEN
      sys_raise_app_error_pkg.raise_user_define_error(p_message_code            => 'CON_CONTRACT_CONTENT_PKG.CON_STATUS_ERROR',
                                                      p_created_by              => p_user_id,
                                                      p_package_name            => 'cus_prj_content_pkg',
                                                      p_procedure_function_name => 'prj_project_check');
      raise_application_error(sys_raise_app_error_pkg.c_error_number,
                              sys_raise_app_error_pkg.g_err_line_id);
  END;
  --删除商业伙伴伙伴已被删除的打印文本
  PROCEDURE delete_when_create_check(p_project_id   NUMBER,
                                     p_quotation_id NUMBER,
                                     p_user_id      NUMBER) IS
  BEGIN

    DELETE FROM prj_project_content t
     WHERE t.project_id = p_project_id
       AND t.quotation_id = p_quotation_id
       AND NOT EXISTS
     (SELECT 1 FROM prj_project_bp d WHERE d.prj_bp_id = t.prj_bp_id);
  END;

  FUNCTION prj_tmpt_check(p_project_id  NUMBER,
                          p_tmpt_id     NUMBER,
                          p_user_id     NUMBER,
                          p_bp_id       NUMBER,
                          p_bp_class    VARCHAR2,
                          p_bp_category VARCHAR2) RETURN VARCHAR2 IS
    v_count        NUMBER;
    v_exist        NUMBER;
    v_nationality  VARCHAR2(30); --国籍
    v_country_code VARCHAR2(30);
  BEGIN
    SELECT COUNT(*)
      INTO v_count
      FROM con_contract_tmpt_clause c
     WHERE c.tmpt_id = p_tmpt_id;
    IF v_count = 0 THEN
      RETURN g_check_yes;
    END IF;
    BEGIN
      SELECT nvl(fc.country_code, 'CHN')
        INTO v_country_code
        FROM hls_bp_master bm, fnd_country fc
       WHERE bm.nationality = fc.country_id(+)
         AND bm.bp_id = p_bp_id;
    EXCEPTION
      WHEN OTHERS THEN
        v_country_code := 'CHN';
    END;
    IF v_country_code = 'CHN' THEN
      v_nationality := 'inland';
    ELSE
      v_nationality := 'foreign';
    END IF;

    FOR cv IN (SELECT *
                 FROM con_contract_tmpt_clause c
                WHERE c.tmpt_id = p_tmpt_id) LOOP
      /*SELECT COUNT(*)
       INTO v_exist
       FROM prj_project ct
      WHERE ct.project_id = p_project_id
        AND ct.business_type = nvl(cv.business_type, ct.business_type) --业务类型
        AND ct.lease_organization =
            nvl(cv.lease_organization, ct.lease_organization) --事业部
        AND ct.lease_channel = nvl(cv.lease_channel, ct.lease_channel) --商业模式
        AND ct.document_type = nvl(cv.document_type, ct.document_type) --单据类型
        AND nvl(ct.price_list,'N') = nvl(cv.price_list, nvl(ct.price_list,'N')) --报价方案
        AND ct.division = nvl(cv.division, ct.division) --产品线
        and p_bp_category = nvl(cv.bp_category,p_bp_category)
        and p_bp_class = nvl(cv.bp_class,p_bp_class)
        and v_nationality = nvl(cv.other_tmpt_clause,v_nationality)
        ;*/
      SELECT COUNT(*)
        INTO v_exist
        FROM prj_project ct
       WHERE ct.project_id = p_project_id
         AND cv.data_class = 'NORMAL'
         AND ct.business_type = nvl(cv.business_type, ct.business_type) --业务类型
         AND ct.lease_organization =
             nvl(cv.lease_organization, ct.lease_organization) --事业部
         AND ct.lease_channel = nvl(cv.lease_channel, ct.lease_channel) --商业模式
         AND ct.document_type = nvl(cv.document_type, ct.document_type) --单据类型
         AND nvl(ct.price_list, '##') =
             nvl(cv.price_list, nvl(ct.price_list, '##')) --报价方案
         AND ct.division = nvl(cv.division, ct.division) --产品线
         AND p_bp_category = nvl(cv.bp_category, p_bp_category)
         AND p_bp_class = nvl(cv.bp_class, p_bp_class)
         AND v_nationality = nvl(cv.other_tmpt_clause, v_nationality)
         AND cv.content_type = 'NORMAL';
      IF v_exist = 0 THEN
        NULL;
        --RETURN g_check_no;
      ELSE
        RETURN g_check_yes;
      END IF;
    END LOOP;
    RETURN g_check_no;
  END prj_tmpt_check;

  --插入合同文本
  PROCEDURE insert_contract_content(p_project_id         NUMBER,
                                    p_quotation_id       NUMBER,
                                    p_content_number     VARCHAR2,
                                    p_clause_usage       VARCHAR2,
                                    p_templet_id         NUMBER,
                                    p_prj_bp_id          NUMBER,
                                    p_mortgage_id        NUMBER,
                                    p_content_print_flag VARCHAR2,
                                    p_available_flag     VARCHAR2,
                                    p_user_id            NUMBER,
                                    p_print_num          VARCHAR2) IS
    r_prj_project_content prj_project_content%ROWTYPE;
  BEGIN
    r_prj_project_content.content_id         := prj_project_content_s.nextval;
    r_prj_project_content.project_id         := p_project_id;
    r_prj_project_content.quotation_id       := p_quotation_id;
    r_prj_project_content.clause_usage       := p_clause_usage;
    r_prj_project_content.templet_id         := p_templet_id;
    r_prj_project_content.prj_bp_id          := p_prj_bp_id;
    r_prj_project_content.content_print_flag := p_content_print_flag;
    r_prj_project_content.available_flag     := p_available_flag;
    r_prj_project_content.content_number     := p_content_number;
    r_prj_project_content.creation_date      := SYSDATE;
    r_prj_project_content.created_by         := p_user_id;
    r_prj_project_content.last_update_date   := SYSDATE;
    r_prj_project_content.last_updated_by    := p_user_id;
    r_prj_project_content.ref_v05            := p_print_num;
    INSERT INTO prj_project_content VALUES r_prj_project_content;

  END insert_contract_content;
  --插入项目文本附件
  PROCEDURE save_cdd_item_from_doc(p_project_id   NUMBER,
                                   p_quotation_id NUMBER,
                                   p_user_id      NUMBER) IS

    CURSOR cur_records IS

      SELECT ct.templet_code,
             t1.content_id,
             ct.description doc_name,
             t1.check_id,
             ct.tab_group,
             ct.templet_class
        FROM prj_project_content t1,
             sys_code_values_v   sc,
             con_clause_templet  ct
       WHERE t1.project_id = p_project_id
         AND t1.quotation_id = p_quotation_id
         AND sc.code = 'CON_TMPLET_USAGE'
         AND sc.code_value = t1.clause_usage
         AND ct.templet_id = t1.templet_id;

    v_cdd_item NUMBER;

    v_cdd_item_id  NUMBER;
    v_check_id     NUMBER;
    v_cdd_list_id  NUMBER;
    v_tmpt_count   NUMBER;
    v_tab_group_id NUMBER;
    v_groub_number VARCHAR2(30) := 'CONTENT';
  BEGIN

    /*BEGIN
      SELECT tg.tab_group_id
        INTO v_groub_id
        FROM PRJ_CDD_ITEM_TAB_GROUP tg
       WHERE tg.tab_group = 'PRJ_CDD_ITEM_TAB_GROUP';

    EXCEPTION
      WHEN no_data_found THEN
        NULL;
    END;*/

    FOR cur_rec IN cur_records LOOP

      /*SELECT COUNT(*)
       INTO v_tmpt_count
       FROM prj_cdd_item_doc_ref pr,
            prj_cdd_item_check   pc,
            prj_cdd_item         pi
      WHERE pr.check_id = pc.check_id
        AND pc.cdd_item_id = pi.cdd_item_id
        AND pr.document_table = 'PRJ_PROJECT'
        AND pr.document_id = p_project_id
        AND pi.description = cur_rec.doc_name;*/
      IF cur_rec.check_id IS NULL THEN
        SELECT tg.tab_group_id
          INTO v_tab_group_id
          FROM PRJ_CDD_ITEM_TAB_GROUP tg
         WHERE tg.tab_group = nvl(cur_rec.tab_group, 'CONTENT');

        SELECT t.cdd_list_id
          INTO v_cdd_list_id
          FROM prj_project t
         WHERE t.project_id = p_project_id;

        IF v_cdd_list_id IS NOT NULL THEN
          --只有CDD_LIST不为空才会插入附件

          prj_cdd_item_pkg.prj_cdd_item_doc_ref_save(p_document_table          => 'PRJ_PROJECT',
                                                     p_document_id             => p_project_id,
                                                     p_cdd_list_id             => v_cdd_list_id,
                                                     p_cdd_item                => v_cdd_item,
                                                     p_description             => cur_rec.doc_name,
                                                     p_line_type               => 'FILE',
                                                     p_order_seq               => '',
                                                     p_show_seq                => '',
                                                     p_enabled_flag            => 'Y',
                                                     p_chance_required_flag    => 'N',
                                                     p_chance_display_flag     => 'N',
                                                     p_project_required_flag   => 'N',
                                                     p_project_display_flag    => 'N',
                                                     p_contract_required_flag  => 'N',
                                                     p_contract_display_flag   => 'N',
                                                     p_sign_required_flag      => 'N',
                                                     p_sign_display_flag       => 'N',
                                                     p_incept_required_flag    => 'N',
                                                     p_incept_display_flag     => 'N',
                                                     p_lender_required_flag    => 'Y',
                                                     p_lender_display_flag     => 'Y',
                                                     p_chance_tab_group        => '',
                                                     p_project_tab_group       => '',
                                                     p_contract_tab_group      => NULL,
                                                     p_sign_tab_group          => NULL,
                                                     p_incept_tab_group        => NULL,
                                                     p_lender_tab_group        => '01',
                                                     p_sys_flag                => 'N',
                                                     p_note                    => '',
                                                     p_send_flag               => '',
                                                     p_paper_required          => '',
                                                     p_attachment_required     => '',
                                                     p_not_applicable          => '',
                                                     p_cdd_item_id             => v_cdd_item_id,
                                                     p_check_id                => v_check_id,
                                                     p_user_id                 => p_user_id,
                                                     p_attachment_tab_group_id => v_tab_group_id);
          UPDATE prj_project_content ct
             SET ct.check_id = v_check_id
           WHERE ct.content_id = cur_rec.content_id;
        END IF;
        update prj_cdd_item pci
           set pci.templet_class = cur_rec.templet_class
        where pci.cdd_item_id = v_cdd_item_id;
        v_cdd_item_id := NULL;
        v_cdd_item    := NULL;
        v_check_id    := NULL;
      END IF;
    END LOOP;
  END;

  --生成合同文本本
  PROCEDURE prj_content_create(p_project_id   NUMBER,
                               p_quotation_id NUMBER,
                               p_user_id      NUMBER) IS
    r_prj_rec    prj_project%ROWTYPE;
    v_prj_bp_id  NUMBER;
    v_tmpt_count NUMBER;
    r_con_clause_templet con_clause_templet%ROWTYPE;
    v_customer_service_fee  number;
  BEGIN
    /*未来可以增加状态校验*/
    prj_project_check(p_project_id, p_user_id, r_prj_rec);

    --删除已经不存在的商业伙伴对应的文本
    delete_when_create_check(p_project_id, p_quotation_id, p_user_id);

    FOR cv IN (SELECT * FROM con_clause_templet t WHERE t.enabled_flag = 'Y') LOOP
      --处理承租人，因为多报价承租人也只有一个
      FOR c_prj_bps IN (SELECT cb.prj_bp_id,
                               cb.bp_id,
                               cb.bp_category,
                               cb.bp_class

                          FROM prj_project    ct,
                               prj_project_bp cb,
                               prj_quotation  quo
                         WHERE ct.project_id = p_project_id
                           AND cb.project_id = ct.project_id
                           and cb.bp_category = 'TENANT'
                              /*看是否要放bp_id_tenant到头表上*/
                           --AND cb.contract_seq = quo.contract_seq
                           AND quo.quotation_id = p_quotation_id
                        --AND cb.bp_id = ct.bp_id_tenant
                        /*===================*/
                        ) LOOP
        IF prj_tmpt_check(p_project_id,
                          cv.templet_id,
                          p_user_id,
                          c_prj_bps.bp_id,
                          c_prj_bps.bp_class,
                          c_prj_bps.bp_category) = g_check_yes THEN
          --end if;
          SELECT COUNT(*)
            INTO v_tmpt_count
            FROM prj_project_content cc
           WHERE cc.project_id = p_project_id
             AND cc.quotation_id = p_quotation_id
             and cc.prj_bp_id = c_prj_bps.prj_bp_id
             AND cc.templet_id = cv.templet_id;
          IF v_tmpt_count = 0 THEN
            insert_contract_content(p_project_id         => p_project_id,
                                    p_quotation_id       => p_quotation_id,
                                    p_content_number     => cv.description,
                                    p_clause_usage       => cv.templet_usage,
                                    p_templet_id         => cv.templet_id,
                                    p_prj_bp_id          => c_prj_bps.prj_bp_id,
                                    p_mortgage_id        => '',
                                    p_content_print_flag => 'N',
                                    p_available_flag     => 'Y',
                                    p_user_id            => p_user_id,
                                    p_print_num          => cv.print_num);

          END IF;
        END IF;
      END LOOP;

      --处理非承租人
      FOR c_prj_bps IN (SELECT cb.prj_bp_id,
                               cb.bp_id,
                               cb.bp_category,
                               cb.bp_class

                          FROM prj_project    ct,
                               prj_project_bp cb,
                               prj_quotation  quo
                         WHERE ct.project_id = p_project_id
                           AND cb.project_id = ct.project_id
                           and cb.bp_category !='TENANT'
                              /*看是否要放bp_id_tenant到头表上*/
                           AND cb.contract_seq = quo.contract_seq
                           AND quo.quotation_id = p_quotation_id
                        --AND cb.bp_id = ct.bp_id_tenant
                        /*===================*/
                        ) LOOP
        IF prj_tmpt_check(p_project_id,
                          cv.templet_id,
                          p_user_id,
                          c_prj_bps.bp_id,
                          c_prj_bps.bp_class,
                          c_prj_bps.bp_category) = g_check_yes THEN
          --end if;
          SELECT COUNT(*)
            INTO v_tmpt_count
            FROM prj_project_content cc
           WHERE cc.project_id = p_project_id
             AND cc.quotation_id = p_quotation_id
             and cc.prj_bp_id = c_prj_bps.prj_bp_id
             AND cc.templet_id = cv.templet_id;
          IF v_tmpt_count = 0 THEN
            insert_contract_content(p_project_id         => p_project_id,
                                    p_quotation_id       => p_quotation_id,
                                    p_content_number     => cv.description,
                                    p_clause_usage       => cv.templet_usage,
                                    p_templet_id         => cv.templet_id,
                                    p_prj_bp_id          => c_prj_bps.prj_bp_id,
                                    p_mortgage_id        => '',
                                    p_content_print_flag => 'N',
                                    p_available_flag     => 'Y',
                                    p_user_id            => p_user_id,
                                    p_print_num          => cv.print_num);

          END IF;
        END IF;
      END LOOP;

      /*特殊文本生成 */
      BEGIN
        SELECT *
          INTO r_con_clause_templet
          FROM con_clause_templet ct
         WHERE ct.templet_code = 'CUS_CSA';

        SELECT COUNT(*)
            INTO v_tmpt_count
            FROM prj_project_content cc
           WHERE cc.project_id = p_project_id
             AND cc.quotation_id = p_quotation_id
             AND cc.templet_id = r_con_clause_templet.templet_id;

         select
           ch.customer_service_fee
           into v_customer_service_fee
         from hls_fin_calculator_hd ch,prj_quotation quo
         where quo.quotation_id = p_quotation_id
         and quo.calc_session_id = ch.calc_session_id;

        IF v_tmpt_count = 0 THEN
          IF  (r_prj_rec.business_type = 'LEASE' and v_customer_service_fee >0
                and (r_prj_rec.lease_channel = '41' and r_prj_rec.division ='20')
                )  then
            SELECT cp.prj_bp_id
              INTO v_prj_bp_id
              FROM prj_project_bp cp
             WHERE cp.project_id = p_project_id
               AND cp.bp_category = 'TENANT';

            insert_contract_content(p_project_id         => p_project_id,
                                    p_quotation_id       => p_quotation_id,
                                    p_content_number     => r_con_clause_templet.description,
                                    p_clause_usage       => r_con_clause_templet.templet_usage,
                                    p_templet_id         => r_con_clause_templet.templet_id,
                                    p_prj_bp_id          => v_prj_bp_id,
                                    p_mortgage_id        => '',
                                    p_content_print_flag => 'N',
                                    p_available_flag     => 'Y',
                                    p_user_id            => p_user_id,
                                    p_print_num          => r_con_clause_templet.print_num);

          END IF;
        END IF;
      EXCEPTION
        WHEN no_data_found THEN
          NULL;
      END;

    END LOOP;

    /*插入项目文本附件*/
    save_cdd_item_from_doc(p_project_id, p_quotation_id, p_user_id);

    --一些表格的特殊处理
    /* cus_prj_project_content_pkg.prj_content_preprocess(p_session_id   => p_session_id,
    p_project_id   => p_project_id,
    p_quotation_id => p_quotation_id,
    p_user_id      => p_user_id);*/

  END prj_content_create;

  PROCEDURE prj_content_print(p_project_id   NUMBER,
                              p_quotation_id NUMBER,
                              p_content_id   NUMBER,
                              p_file_path    VARCHAR2,
                              p_file_name    VARCHAR2,
                              p_type         VARCHAR2,
                              p_user_id      NUMBER) IS
    r_prj_project_content prj_project_content%ROWTYPE;
    e_lock_table EXCEPTION;
    v_file_name VARCHAR2(200);
  BEGIN
    SELECT decode(p_type, NULL, p_file_name, p_file_name || '.' || p_type)
      INTO v_file_name
      FROM dual;
    SELECT *
      INTO r_prj_project_content
      FROM prj_project_content
     WHERE project_id = p_project_id
       AND quotation_id = p_quotation_id
       AND content_id = p_content_id
       FOR UPDATE NOWAIT;

    --没做验证
    UPDATE prj_project_content
       SET content_print_flag = 'Y',
           file_path          = p_file_path,
           file_name          = v_file_name,
           last_update_date   = SYSDATE,
           last_updated_by    = p_user_id,
           print_date         = SYSDATE
     WHERE project_id = p_project_id
       AND quotation_id = p_quotation_id
       AND content_id = p_content_id;

    COMMIT;

  EXCEPTION
    WHEN e_lock_table THEN
      sys_raise_app_error_pkg.raise_user_define_error(p_message_code            => 'CON_CONTRACT_PKG.CONTRACT_LOCK_ERROR',
                                                      p_created_by              => p_user_id,
                                                      p_package_name            => 'CUS_PRJ_CONTENT_PKG',
                                                      p_procedure_function_name => 'PRJ_CONTENT_PRINT');
      raise_application_error(sys_raise_app_error_pkg.c_error_number,
                              sys_raise_app_error_pkg.g_err_line_id);

    WHEN OTHERS THEN
      sys_raise_app_error_pkg.raise_sys_others_error(p_message                 => dbms_utility.format_error_backtrace || ' ' ||
                                                                                  SQLERRM,
                                                     p_created_by              => p_user_id,
                                                     p_package_name            => 'CUS_PRJ_CONTENT_PKG',
                                                     p_procedure_function_name => 'PRJ_CONTENT_PRINT');

      raise_application_error(sys_raise_app_error_pkg.c_error_number,
                              sys_raise_app_error_pkg.g_err_line_id);
  END prj_content_print;

  PROCEDURE get_prj_content_rec(p_content_id      prj_project_content.content_id%TYPE,
                                p_user_id         prj_project_content.created_by%TYPE,
                                p_prj_content_rec OUT prj_project_content%ROWTYPE) IS
  BEGIN
    SELECT *
      INTO p_prj_content_rec
      FROM prj_project_content d
     WHERE d.content_id = p_content_id
       FOR UPDATE NOWAIT;
  EXCEPTION
    WHEN e_lock_error THEN
      sys_raise_app_error_pkg.raise_user_define_error(p_message_code            => 'CON_CONTRACT_CONTENT_PKG.CON_CONTENT_LOCK',
                                                      p_created_by              => p_user_id,
                                                      p_package_name            => 'con_contract_content_pkg',
                                                      p_procedure_function_name => 'get_contract_content_rec');
      raise_application_error(sys_raise_app_error_pkg.c_error_number,
                              sys_raise_app_error_pkg.g_err_line_id);
  END;

  PROCEDURE delete_prj_content(p_content_id prj_project_content.content_id%TYPE,
                               p_user_id    prj_project_content.created_by%TYPE) IS
    v_prj_content_rec prj_project_content%ROWTYPE;
  BEGIN
    get_prj_content_rec(p_content_id      => p_content_id,
                        p_user_id         => p_user_id,
                        p_prj_content_rec => v_prj_content_rec);

        DELETE FROM prj_cdd_item_check ic
     WHERE ic.check_id = v_prj_content_rec.check_id;
    DELETE FROM prj_cdd_item_doc_ref dr
     WHERE dr.check_id = v_prj_content_rec.check_id;

    DELETE FROM prj_project_content d WHERE d.content_id = p_content_id;
  END;

END cus_prj_content_pkg;
/
