-- Create table
create table HLS_DOC_FILE_TEMPLET_PARA
(
  TEMPLET_PARA_ID      NUMBER not null,
  BOOKMARK             VARCHAR2(100) not null,
  BOOKMARK_DESCRIPTION VARCHAR2(2000),
  SQL_CONTENT          CLOB,
  ENABLED_FLAG         VARCHAR2(1),
  CREATION_DATE        DATE not null,
  CREATED_BY           NUMBER not null,
  LAST_UPDATE_DATE     DATE not null,
  LAST_UPDATED_BY      NUMBER not null,
  BOOKMARK_TYPE        VARCHAR2(30),
  SQL                  CLOB,
  FONT_FAMILY          VARCHAR2(30),
  FONT_SIZE            NUMBER,
  TABLE_WIDTH          NUMBER,
  IND_WIDTH            NUMBER,
  UNDERLINE            VARCHAR2(30),
  LINE_NUM             NUMBER
);
-- Create/Recreate primary, unique and foreign key constraints
alter table HLS_DOC_FILE_TEMPLET_PARA
  add constraint HLS_DOC_FILE_TEMPLET_PARA_PK primary key (TEMPLET_PARA_ID)
  using index ;
-- Create/Recreate indexes
create unique index HLS_DOC_FILE_TEMPLET_PARA_U1 on HLS_DOC_FILE_TEMPLET_PARA (BOOKMARK);
