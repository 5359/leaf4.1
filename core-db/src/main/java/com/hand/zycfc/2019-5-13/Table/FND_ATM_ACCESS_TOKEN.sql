-- Create table
create table FND_ATM_ACCESS_TOKEN
(
  RECORD_ID           NUMBER not null,
  ATTACHMENT_ID       NUMBER,
  ACCESS_TOKEN        VARCHAR2(200),
  EXPIRED_READ_FLAG   VARCHAR2(1),
  EXPIRED_WRITE_FLAG  VARCHAR2(1),
  EXPIRED_WRITE_TIMES NUMBER,
  CREATION_DATE       DATE not null,
  CREATED_BY          NUMBER not null,
  LAST_UPDATE_DATE    DATE not null,
  LAST_UPDATED_BY     NUMBER not null
);
-- Create/Recreate primary, unique and foreign key constraints
alter table FND_ATM_ACCESS_TOKEN
  add constraint FND_ATM_ACCESS_TOKEN_PK primary key (RECORD_ID)
  using index ;
-- Create/Recreate indexes
create unique index FND_ATM_ACCESS_TOKEN_U1 on FND_ATM_ACCESS_TOKEN (ATTACHMENT_ID, ACCESS_TOKEN);
