-- Create table
create table HLS_DOC_FILE_TEMPLET_SET_LN
(
  TEMPLET_SET_LN_ID NUMBER not null,
  TEMPLET_SET_ID    NUMBER not null,
  TEMPLET_ID        NUMBER not null,
  ENABLED_FLAG      VARCHAR2(1),
  CREATION_DATE     DATE not null,
  CREATED_BY        NUMBER not null,
  LAST_UPDATE_DATE  DATE not null,
  LAST_UPDATED_BY   NUMBER not null
);
-- Add comments to the columns
comment on column HLS_DOC_FILE_TEMPLET_SET_LN.TEMPLET_SET_ID
  is '选择模板集';
comment on column HLS_DOC_FILE_TEMPLET_SET_LN.TEMPLET_ID
  is '选择模板';
-- Create/Recreate primary, unique and foreign key constraints
alter table HLS_DOC_FILE_TEMPLET_SET_LN
  add constraint HLS_DOC_FILE_TEMPLET_SET_LN_PK primary key (TEMPLET_SET_LN_ID)
  using index ;
-- Create/Recreate indexes
create unique index HLS_DOC_FILE_TEMPLET_SET_LN_U1 on HLS_DOC_FILE_TEMPLET_SET_LN (TEMPLET_SET_ID, TEMPLET_ID);
