CREATE OR REPLACE PACKAGE hls_doc_file_templet_pkg IS

  -- Author  : gaoyang
  -- Created : 2015-6-10 13:42:39
  -- Purpose :

  FUNCTION get_doc_bookmark_value
  (
    p_content_id NUMBER,
    p_bookmark   VARCHAR2,
    p_user_id    NUMBER,
    p_type       VARCHAR2 DEFAULT 'CONTRACT'
  ) RETURN CLOB;

  PROCEDURE office_insert_fnd_atm
  (
    p_record_id      OUT fnd_atm_attachment_multi.record_id%TYPE,
    p_table_name     fnd_atm_attachment_multi.table_name%TYPE,
    p_table_pk_value fnd_atm_attachment_multi.table_pk_value%TYPE,
    p_file_name      fnd_atm_attachment.file_name%TYPE,
    p_file_path      fnd_atm_attachment.file_path%TYPE,
    p_user_id        fnd_atm_attachment_multi.created_by%TYPE,
    p_source_type    VARCHAR2 DEFAULT 'CONTRACT'
  );

  PROCEDURE office_insert_fnd_atm_prj
  (
    p_record_id      OUT fnd_atm_attachment_multi.record_id%TYPE,
    p_table_name     fnd_atm_attachment_multi.table_name%TYPE,
    p_table_pk_value fnd_atm_attachment_multi.table_pk_value%TYPE,
    p_file_name      fnd_atm_attachment.file_name%TYPE,
    p_file_path      fnd_atm_attachment.file_path%TYPE,
    p_user_id        fnd_atm_attachment_multi.created_by%TYPE
  );
  FUNCTION get_doc_bookmark_value_new
  (
    p_session_id NUMBER,
    p_content_id NUMBER,
    p_bookmark   VARCHAR2,
    p_user_id    NUMBER,
    p_type       VARCHAR2 DEFAULT 'CONTRACT'
  ) RETURN CLOB;

  PROCEDURE edit_para_table_column
  (
    p_templet_para_id NUMBER,
    p_column_name     VARCHAR2,
    p_column_num      NUMBER,
    p_user_id         NUMBER
  );

  PROCEDURE upd_atm_access_token
  (
    p_attachment_id NUMBER,
    p_access_type   VARCHAR2,
    p_user_id       NUMBER
  );

  FUNCTION get_atm_access_token
  (
    p_attachment_id NUMBER,
    p_user_id       NUMBER
  ) RETURN VARCHAR2;
  /*add by xuls 2017/10/21*/
  FUNCTION get_doc_bookmark_value_common
  (
    p_session_id NUMBER,
    p_content_id NUMBER,
    p_bookmark   VARCHAR2,
    p_user_id    NUMBER
  ) RETURN CLOB;
  PROCEDURE ins_hls_doc_file_content
  (
    p_content_id     OUT hls_doc_file_content.content_id%TYPE,
    p_document_id    hls_doc_file_content.document_id%TYPE,
    p_document_table hls_doc_file_content.document_table%TYPE,
    p_clause_usage   hls_doc_file_content.clause_usage%TYPE,
    p_templet_id     hls_doc_file_content.templet_id%TYPE,
    p_user_id        hls_doc_file_content.created_by%TYPE,
    p_ref_v01        hls_doc_file_content.ref_v01%TYPE DEFAULT NULL,
    p_ref_v02        hls_doc_file_content.ref_v02%TYPE DEFAULT NULL,
    p_ref_v03        hls_doc_file_content.ref_v03%TYPE DEFAULT NULL,
    p_ref_v04        hls_doc_file_content.ref_v04%TYPE DEFAULT NULL,
    p_ref_v05        hls_doc_file_content.ref_v05%TYPE DEFAULT NULL,
    p_ref_n01        hls_doc_file_content.ref_n01%TYPE DEFAULT NULL,
    p_ref_n02        hls_doc_file_content.ref_n02%TYPE DEFAULT NULL,
    p_ref_n03        hls_doc_file_content.ref_n03%TYPE DEFAULT NULL,
    p_ref_n04        hls_doc_file_content.ref_n04%TYPE DEFAULT NULL,
    p_ref_n05        hls_doc_file_content.ref_n05%TYPE DEFAULT NULL,
    p_ref_d01        hls_doc_file_content.ref_d01%TYPE DEFAULT NULL,
    p_ref_d02        hls_doc_file_content.ref_d02%TYPE DEFAULT NULL,
    p_ref_d03        hls_doc_file_content.ref_d03%TYPE DEFAULT NULL,
    p_ref_d04        hls_doc_file_content.ref_d04%TYPE DEFAULT NULL,
    p_ref_d05        hls_doc_file_content.ref_d05%TYPE DEFAULT NULL
  );
  
  
  /*删除已生成的租金通知书 hongquan.dai 20180829*/
  PROCEDURE delete_hls_doc_file_content
  (p_user_id     NUMBER,
   p_cashflow_id NUMBER
  );
    
             
END hls_doc_file_templet_pkg;

 
/
CREATE OR REPLACE PACKAGE BODY hls_doc_file_templet_pkg IS

  FUNCTION get_bookmark_value
  (
    p_content_id   NUMBER,
    p_contract_id  NUMBER,
    p_project_id   NUMBER,
    p_quotation_id NUMBER,
    p_bp_id        NUMBER,
    p_bookmark     VARCHAR2,
    p_user_id      NUMBER
  ) RETURN CLOB IS
    v_sql     VARCHAR2(32767);
    v_value   CLOB;
    v_sql_str VARCHAR2(32767);
    e_field_error EXCEPTION;
  BEGIN
    IF p_contract_id IS NULL THEN
      --如果是合同文本打印 执行
      BEGIN
        SELECT d.sql_content
          INTO v_sql
          FROM hls_doc_file_templet_para d
         WHERE d.bookmark = p_bookmark
               AND d.enabled_flag = 'Y'
               AND d.bookmark_type IN ('TEXT', 'TABLE_AREA', 'TEXTAREA');
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RETURN ' ';
      END;
    ELSE
      BEGIN
        SELECT d.SQL
          INTO v_sql
          FROM hls_doc_file_templet_para d
         WHERE d.bookmark = p_bookmark
               AND d.enabled_flag = 'Y'
               AND d.bookmark_type IN ('TEXT', 'TABLE_AREA', 'TEXTAREA');
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RETURN ' ';
      END;
    
    END IF;
  
    v_sql := REPLACE(v_sql,
                     '{$CONTRACT_ID$}',
                     p_contract_id);
    v_sql := REPLACE(v_sql,
                     '{$CONTENT_ID$}',
                     p_content_id);
    v_sql := REPLACE(v_sql,
                     '{$PROJECT_ID$}',
                     p_project_id);
    v_sql := REPLACE(v_sql,
                     '{$QUOTATION_ID$}',
                     p_quotation_id);
    v_sql := REPLACE(v_sql,
                     '{$BP_ID$}',
                     p_bp_id);
  
    BEGIN
      IF v_sql IS NULL THEN
        RETURN '';
      END IF;
      IF instr(upper(v_sql),
               'SELECT') != 0 THEN
        EXECUTE IMMEDIATE v_sql
          INTO v_value;
      ELSE
        v_sql_str := 'begin :v_value := ' || v_sql || '; end;';
        EXECUTE IMMEDIATE v_sql_str
          USING OUT v_value;
      END IF;
      IF v_value IS NULL THEN
        v_value := '        ';
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_value := '        ';
      WHEN OTHERS THEN
        sys_raise_app_error_pkg.raise_sys_others_error(p_message                 => dbms_utility.format_error_backtrace ||
                                                                                    SQLERRM || v_sql,
                                                       p_created_by              => p_user_id,
                                                       p_package_name            => 'hls_doc_file_templet_pkg',
                                                       p_procedure_function_name => 'get_bookmark_value');
        raise_application_error(sys_raise_app_error_pkg.c_error_number,
                                sys_raise_app_error_pkg.g_err_line_id);
    END;
    RETURN v_value;
  
  END;
  FUNCTION get_bookmark_value_new
  (
    p_session_id   NUMBER,
    p_content_id   NUMBER,
    p_contract_id  NUMBER,
    p_project_id   NUMBER,
    p_quotation_id NUMBER,
    p_bp_id        NUMBER,
    p_bookmark     VARCHAR2,
    p_user_id      NUMBER
  ) RETURN CLOB IS
    v_sql     VARCHAR2(32767);
    v_value   CLOB;
    v_sql_str VARCHAR2(32767);
    e_field_error EXCEPTION;
  BEGIN
  
    IF p_contract_id IS NULL THEN
      --如果是合同文本打印 执行
      BEGIN
        SELECT d.sql_content
          INTO v_sql
          FROM hls_doc_file_templet_para d
         WHERE d.bookmark = p_bookmark
               AND d.enabled_flag = 'Y'
               AND d.bookmark_type IN ('TEXT', 'TABLE_AREA', 'TEXTAREA');
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RETURN ' ';
      END;
    ELSE
      BEGIN
        SELECT d.SQL
          INTO v_sql
          FROM hls_doc_file_templet_para d
         WHERE d.bookmark = p_bookmark
               AND d.enabled_flag = 'Y'
               AND d.bookmark_type IN ('TEXT', 'TABLE_AREA', 'TEXTAREA');
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RETURN ' ';
      END;
    
    END IF;
    v_sql := REPLACE(v_sql,
                     '{$SESSION_ID$}',
                     p_session_id);
    v_sql := REPLACE(v_sql,
                     '{$CONTRACT_ID$}',
                     p_contract_id);
    v_sql := REPLACE(v_sql,
                     '{$CONTENT_ID$}',
                     p_content_id);
    v_sql := REPLACE(v_sql,
                     '{$PROJECT_ID$}',
                     p_project_id);
    v_sql := REPLACE(v_sql,
                     '{$QUOTATION_ID$}',
                     p_quotation_id);
    v_sql := REPLACE(v_sql,
                     '{$BP_ID$}',
                     p_bp_id);
  
    BEGIN
      IF v_sql IS NULL THEN
        RETURN '';
      END IF;
    
      IF instr(upper(v_sql),
               'SELECT') != 0 THEN
        EXECUTE IMMEDIATE v_sql
          INTO v_value;
      ELSE
        v_sql_str := 'begin :v_value := ' || v_sql || '; end;';
        EXECUTE IMMEDIATE v_sql_str
          USING OUT v_value;
      END IF;
      IF v_value IS NULL THEN
        v_value := '        ';
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_value := '        ';
      WHEN OTHERS THEN
        sys_raise_app_error_pkg.raise_sys_others_error(p_message                 => dbms_utility.format_error_backtrace ||
                                                                                    SQLERRM || v_sql,
                                                       p_created_by              => p_user_id,
                                                       p_package_name            => 'hls_doc_file_templet_pkg',
                                                       p_procedure_function_name => 'get_bookmark_value');
        raise_application_error(sys_raise_app_error_pkg.c_error_number,
                                sys_raise_app_error_pkg.g_err_line_id);
    END;
    RETURN v_value;
  
  END;

  FUNCTION get_doc_bookmark_value
  (
    p_content_id NUMBER,
    p_bookmark   VARCHAR2,
    p_user_id    NUMBER,
    p_type       VARCHAR2 DEFAULT 'CONTRACT'
  ) RETURN CLOB IS
    v_content              CLOB;
    v_con_contract_content con_contract_content%ROWTYPE;
    v_prj_project_contrent prj_project_content%ROWTYPE;
    v_bp_id                NUMBER;
  BEGIN
    IF p_type = 'CONTRACT' THEN
      --如果是合同，则从合同文本表去获取
      SELECT * INTO v_con_contract_content FROM con_contract_content cc WHERE cc.content_id = p_content_id;
      SELECT cb.bp_id
        INTO v_bp_id
        FROM con_contract_bp cb
       WHERE cb.record_id = v_con_contract_content.con_contract_bp_id;
    
    ELSE
      SELECT * INTO v_prj_project_contrent FROM prj_project_content pc WHERE pc.content_id = p_content_id;
      SELECT cb.bp_id INTO v_bp_id FROM prj_project_bp cb WHERE cb.prj_bp_id = v_prj_project_contrent.prj_bp_id;
    END IF;
  
    v_content := get_bookmark_value(p_content_id   => p_content_id,
                                    p_contract_id  => v_con_contract_content.contract_id,
                                    p_project_id   => v_prj_project_contrent.project_id,
                                    p_quotation_id => v_prj_project_contrent.quotation_id,
                                    p_bp_id        => v_bp_id,
                                    p_bookmark     => p_bookmark,
                                    p_user_id      => p_user_id);
    RETURN v_content;
  
  END;

  FUNCTION get_doc_bookmark_value_new
  (
    p_session_id NUMBER,
    p_content_id NUMBER,
    p_bookmark   VARCHAR2,
    p_user_id    NUMBER,
    p_type       VARCHAR2 DEFAULT 'CONTRACT'
  ) RETURN CLOB IS
    v_content              CLOB;
    v_con_contract_content con_contract_content%ROWTYPE;
    v_prj_project_contrent prj_project_content%ROWTYPE;
    v_bp_id                NUMBER;
  BEGIN
    IF p_type = 'CONTRACT' THEN
      --如果是合同，则从合同文本表去获取
      SELECT * INTO v_con_contract_content FROM con_contract_content cc WHERE cc.content_id = p_content_id;
      SELECT cb.bp_id
        INTO v_bp_id
        FROM con_contract_bp cb
       WHERE cb.record_id = v_con_contract_content.con_contract_bp_id;
    
    ELSE
      SELECT * INTO v_prj_project_contrent FROM prj_project_content pc WHERE pc.content_id = p_content_id;
      SELECT cb.bp_id INTO v_bp_id FROM prj_project_bp cb WHERE cb.prj_bp_id = v_prj_project_contrent.prj_bp_id;
    END IF;
  
    v_content := get_bookmark_value_new(p_session_id   => p_session_id,
                                        p_content_id   => p_content_id,
                                        p_contract_id  => v_con_contract_content.contract_id,
                                        p_project_id   => v_prj_project_contrent.project_id,
                                        p_quotation_id => v_prj_project_contrent.quotation_id,
                                        p_bp_id        => v_bp_id,
                                        p_bookmark     => p_bookmark,
                                        p_user_id      => p_user_id);
    RETURN v_content;
  
  END;

  PROCEDURE insert_fnd_atm
  (
    p_attachment_id    OUT fnd_atm_attachment.attachment_id%TYPE,
    p_source_type_code fnd_atm_attachment.source_type_code%TYPE,
    p_source_pk_value  fnd_atm_attachment.source_pk_value%TYPE,
    p_content          fnd_atm_attachment.content%TYPE DEFAULT NULL,
    p_file_type_code   fnd_atm_attachment.file_type_code%TYPE,
    p_mime_type        fnd_atm_attachment.mime_type%TYPE,
    p_file_name        fnd_atm_attachment.file_name%TYPE,
    p_file_size        fnd_atm_attachment.file_size%TYPE,
    p_file_path        fnd_atm_attachment.file_path%TYPE,
    p_user_id          fnd_atm_attachment.created_by%TYPE
  ) IS
  BEGIN
  
    SELECT fnd_atm_attachment_s.NEXTVAL INTO p_attachment_id FROM dual;
  
    INSERT INTO fnd_atm_attachment
      (attachment_id,
       source_type_code,
       source_pk_value,
       content,
       file_type_code,
       mime_type,
       file_name,
       file_size,
       file_path,
       creation_date,
       created_by,
       last_update_date,
       last_updated_by)
    VALUES
      (p_attachment_id,
       p_source_type_code,
       p_source_pk_value,
       p_content,
       p_file_type_code,
       p_mime_type,
       p_file_name,
       p_file_size,
       p_file_path,
       SYSDATE,
       p_user_id,
       SYSDATE,
       p_user_id);
  END;

  PROCEDURE office_insert_fnd_atm
  (
    p_record_id      OUT fnd_atm_attachment_multi.record_id%TYPE,
    p_table_name     fnd_atm_attachment_multi.table_name%TYPE,
    p_table_pk_value fnd_atm_attachment_multi.table_pk_value%TYPE,
    p_file_name      fnd_atm_attachment.file_name%TYPE,
    p_file_path      fnd_atm_attachment.file_path%TYPE,
    p_user_id        fnd_atm_attachment_multi.created_by%TYPE,
    p_source_type    VARCHAR2 DEFAULT 'CONTRACT'
  ) IS
    v_record               fnd_atm_attachment_multi%ROWTYPE;
    v_attachment_id        fnd_atm_attachment.attachment_id%TYPE;
    v_orign_attachment_id  NUMBER;
    v_con_contract_content con_contract_content%ROWTYPE;
  BEGIN
    SELECT t.attachment_id
      INTO v_orign_attachment_id
      FROM fnd_atm_attachment_multi t
     WHERE t.table_name = p_table_name
           AND t.table_pk_value = p_table_pk_value;
    RETURN;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      SELECT fnd_atm_attachment_multi_s.NEXTVAL INTO p_record_id FROM dual;
      insert_fnd_atm(p_attachment_id    => v_attachment_id,
                     p_source_type_code => 'fnd_atm_attachment_multi',
                     p_source_pk_value  => p_record_id,
                     p_file_type_code   => 'doc',
                     p_mime_type        => 'application/msword',
                     p_file_name        => p_file_name,
                     p_file_size        => 9999999,
                     p_file_path        => p_file_path,
                     p_user_id          => p_user_id);
    
      v_record.record_id        := p_record_id;
      v_record.table_name       := p_table_name;
      v_record.table_pk_value   := p_table_pk_value;
      v_record.attachment_id    := v_attachment_id;
      v_record.creation_date    := SYSDATE;
      v_record.created_by       := p_user_id;
      v_record.last_update_date := SYSDATE;
      v_record.last_updated_by  := p_user_id;
    
      INSERT INTO fnd_atm_attachment_multi VALUES v_record;
      IF nvl(p_source_type,
             'CONTRACT') = 'CONTRACT' THEN
        SELECT * INTO v_con_contract_content FROM con_contract_content cc WHERE cc.content_id = p_table_pk_value;
      
        con_contract_pkg.contract_print(p_contract_id => v_con_contract_content.contract_id,
                                        p_content_id  => v_con_contract_content.content_id,
                                        p_file_path   => p_file_path,
                                        p_file_name   => p_file_name,
                                        p_type        => 'doc',
                                        p_user_id     => p_user_id);
      ELSIF nvl(p_source_type,
                'CONTRACT') = 'PAYMENT_NOTICE' THEN
        UPDATE con_contract_cashflow c
           SET c.print_notice_flag = 'Y'
         WHERE c.cashflow_id IN
               (SELECT h.document_id FROM hls_doc_file_content h WHERE h.content_id = p_table_pk_value);
      
      END IF;
    WHEN too_many_rows THEN
      sys_raise_app_error_pkg.raise_sys_others_error(p_message                 => '此合同文本含有多处附件文档，请联系汉得管理用户！',
                                                     p_created_by              => p_user_id,
                                                     p_package_name            => 'hls_doc_file_templet_pkg',
                                                     p_procedure_function_name => 'office_insert_fnd_atm');
      raise_application_error(sys_raise_app_error_pkg.c_error_number,
                              sys_raise_app_error_pkg.g_err_line_id);
  END;

  PROCEDURE office_insert_fnd_atm_prj
  (
    p_record_id      OUT fnd_atm_attachment_multi.record_id%TYPE,
    p_table_name     fnd_atm_attachment_multi.table_name%TYPE,
    p_table_pk_value fnd_atm_attachment_multi.table_pk_value%TYPE,
    p_file_name      fnd_atm_attachment.file_name%TYPE,
    p_file_path      fnd_atm_attachment.file_path%TYPE,
    p_user_id        fnd_atm_attachment_multi.created_by%TYPE
  ) IS
    v_record               fnd_atm_attachment_multi%ROWTYPE;
    v_attachment_id        fnd_atm_attachment.attachment_id%TYPE;
    v_orign_attachment_id  NUMBER;
    v_con_contract_content prj_project_content%ROWTYPE;
  BEGIN
    SELECT t.attachment_id
      INTO v_orign_attachment_id
      FROM fnd_atm_attachment_multi t
     WHERE t.table_name = p_table_name
           AND t.table_pk_value = p_table_pk_value;
    RETURN;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      SELECT fnd_atm_attachment_multi_s.NEXTVAL INTO p_record_id FROM dual;
      insert_fnd_atm(p_attachment_id    => v_attachment_id,
                     p_source_type_code => 'fnd_atm_attachment_multi',
                     p_source_pk_value  => p_record_id,
                     p_file_type_code   => 'doc',
                     p_mime_type        => 'application/msword',
                     p_file_name        => p_file_name,
                     p_file_size        => 9999999,
                     p_file_path        => p_file_path || p_file_name || '.doc',
                     p_user_id          => p_user_id);
    
      v_record.record_id        := p_record_id;
      v_record.table_name       := p_table_name;
      v_record.table_pk_value   := p_table_pk_value;
      v_record.attachment_id    := v_attachment_id;
      v_record.creation_date    := SYSDATE;
      v_record.created_by       := p_user_id;
      v_record.last_update_date := SYSDATE;
      v_record.last_updated_by  := p_user_id;
    
      INSERT INTO fnd_atm_attachment_multi VALUES v_record;
    
      SELECT * INTO v_con_contract_content FROM prj_project_content cc WHERE cc.content_id = p_table_pk_value;
    
      cus_prj_content_pkg.prj_content_print(p_project_id   => v_con_contract_content.project_id,
                                            p_quotation_id => v_con_contract_content.quotation_id,
                                            p_content_id   => v_con_contract_content.content_id,
                                            p_file_path    => p_file_path,
                                            p_file_name    => p_file_name, --v_con_contract_content.content_number,
                                            p_type         => 'doc',
                                            p_user_id      => p_user_id);
    
    WHEN too_many_rows THEN
      sys_raise_app_error_pkg.raise_sys_others_error(p_message                 => '此合同文本含有多处附件文档，请联系汉得管理用户！',
                                                     p_created_by              => p_user_id,
                                                     p_package_name            => 'hls_doc_file_templet_pkg',
                                                     p_procedure_function_name => 'office_insert_fnd_atm');
      raise_application_error(sys_raise_app_error_pkg.c_error_number,
                              sys_raise_app_error_pkg.g_err_line_id);
  END;

  PROCEDURE edit_para_table_column
  (
    p_templet_para_id NUMBER,
    p_column_name     VARCHAR2,
    p_column_num      NUMBER,
    p_user_id         NUMBER
  ) IS
    r_hls_doc_file_para_table hls_doc_file_para_table%ROWTYPE;
  BEGIN
    SELECT *
      INTO r_hls_doc_file_para_table
      FROM hls_doc_file_para_table t
     WHERE t.column_name = p_column_name
           AND t.templet_para_id = p_templet_para_id
           AND rownum = 1;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      INSERT INTO hls_doc_file_para_table
        (para_table_id,
         templet_para_id,
         column_name,
         column_desc,
         column_num,
         enabled_flag,
         creation_date,
         created_by,
         last_update_date,
         last_updated_by)
      VALUES
        (hls_doc_file_para_table_s.NEXTVAL,
         p_templet_para_id,
         p_column_name,
         p_column_name,
         p_column_num,
         'Y',
         SYSDATE,
         p_user_id,
         SYSDATE,
         p_user_id);
  END;

  PROCEDURE upd_atm_access_token
  (
    p_attachment_id NUMBER,
    p_access_type   VARCHAR2,
    p_user_id       NUMBER
  ) IS
    v_access_token VARCHAR2(200);
  BEGIN
    UPDATE fnd_atm_access_token t
       SET t.expired_read_flag  = 'Y',
           t.expired_write_flag = 'Y',
           t.last_update_date   = SYSDATE,
           t.last_updated_by    = p_user_id
     WHERE t.attachment_id = p_attachment_id
           AND t.expired_read_flag = 'N'
           AND t.expired_write_flag = 'N';
    IF p_access_type = 'READ' THEN
      v_access_token := sys_guid() || '001';
    ELSIF p_access_type = 'WRITE' THEN
      v_access_token := sys_guid() || '002';
    END IF;
  
    INSERT INTO fnd_atm_access_token
      (record_id,
       attachment_id,
       access_token,
       expired_read_flag,
       expired_write_flag,
       expired_write_times,
       creation_date,
       created_by,
       last_update_date,
       last_updated_by)
    VALUES
      (fnd_atm_access_token_s.NEXTVAL,
       p_attachment_id,
       v_access_token,
       'N',
       'N',
       0,
       SYSDATE,
       p_user_id,
       SYSDATE,
       p_user_id);
  END;

  FUNCTION get_atm_access_token
  (
    p_attachment_id NUMBER,
    p_user_id       NUMBER
  ) RETURN VARCHAR2 IS
    v_access_token VARCHAR2(200);
  BEGIN
    SELECT t.access_token
      INTO v_access_token
      FROM fnd_atm_access_token t
     WHERE t.attachment_id = p_attachment_id
           AND t.expired_read_flag = 'N'
           AND t.expired_write_flag = 'N';
  
    RETURN v_access_token;
  END;
  /*add by xuls 2017/10/21*/
  FUNCTION get_bookmark_value_common
  (
    p_session_id     NUMBER,
    p_document_id    NUMBER,
    p_document_table VARCHAR2,
    p_bookmark       VARCHAR2,
    p_user_id        NUMBER
  ) RETURN CLOB IS
    v_sql     VARCHAR2(32767);
    v_value   CLOB;
    v_sql_str VARCHAR2(32767);
    e_field_error EXCEPTION;
  
    p_contract_id NUMBER;
    p_project_id  NUMBER;
    v_cashflow_id NUMBER;
  BEGIN
  
  
    --提前结清
    IF (p_document_table = 'CON_CONTRACT_ET_HD') THEN
      SELECT t.contract_id INTO p_contract_id FROM con_contract_et_hd t WHERE t.et_agreement_id = p_document_id;
    ELSIF (p_document_table = 'CON_CONTRACT_CASHFLOW') THEN
      SELECT ca.contract_id,
             ca.cashflow_id
        INTO p_contract_id,
             v_cashflow_id
        FROM con_contract_cashflow ca
       WHERE ca.cashflow_id = p_document_id;
    END IF;
  
    /*if (p_document_table = 'PROJECT_REPORT') then
      select from prj_project pp where pp.project_id = p
    end if;*/
  
    BEGIN
      SELECT d.SQL
        INTO v_sql
        FROM hls_doc_file_templet_para d
       WHERE d.bookmark = p_bookmark
             AND d.enabled_flag = 'Y'
             AND d.bookmark_type IN ('TEXT', 'TABLE_AREA', 'TEXTAREA');
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RETURN ' ';
    END;
  
    --  end
  
    --如果是尽调报告
    IF (p_document_table = 'PROJECT_REPORT') THEN
      v_sql := REPLACE(v_sql,
                       '{$PROJECT_ID$}',
                       p_document_id);
    END IF;
  
    v_sql := REPLACE(v_sql,
                     '{$SESSION_ID$}',
                     p_session_id);
    v_sql := REPLACE(v_sql,
                     '{$DOCUMENT_ID$}',
                     p_document_id);
    v_sql := REPLACE(v_sql,
                     '{$DOCUMENT_TABLE$}',
                     p_document_table);
    v_sql := REPLACE(v_sql,
                     '{$CONTRACT_ID$}',
                     p_contract_id);
    v_sql := REPLACE(v_sql,
                     '{$CASHFLOW_ID$}',
                     v_cashflow_id);
  
    BEGIN
      IF v_sql IS NULL THEN
        RETURN '';
      END IF;
      IF instr(upper(v_sql),
               'SELECT') != 0 THEN
        EXECUTE IMMEDIATE v_sql
          INTO v_value;
      ELSE
        v_sql_str := 'begin :v_value := ' || v_sql || '; end;';
        EXECUTE IMMEDIATE v_sql_str
          USING OUT v_value;
      END IF;
      IF v_value IS NULL THEN
        v_value := '        ';
      END IF;
      /*      sys_raise_app_error_pkg.raise_sys_others_error(p_message => dbms_utility.format_error_backtrace ||
                   SQLERRM ||
                   v_sql,
      p_created_by => p_user_id,
      p_package_name => 'hls_doc_file_templet_pkg',
      p_procedure_function_name => 'get_bookmark_value_common');*/
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_value := '        ';
      WHEN OTHERS THEN
        sys_raise_app_error_pkg.raise_sys_others_error(p_message                 => dbms_utility.format_error_backtrace ||
                                                                                    SQLERRM || v_sql,
                                                       p_created_by              => p_user_id,
                                                       p_package_name            => 'hls_doc_file_templet_pkg',
                                                       p_procedure_function_name => 'get_bookmark_value_common');
        raise_application_error(sys_raise_app_error_pkg.c_error_number,
                                sys_raise_app_error_pkg.g_err_line_id);
    END;
    RETURN v_value;
  
  END;
  /*add by xuls 2017/10/21*/
  FUNCTION get_doc_bookmark_value_common
  (
    p_session_id NUMBER,
    p_content_id NUMBER,
    p_bookmark   VARCHAR2,
    p_user_id    NUMBER
  ) RETURN CLOB IS
    v_content                  CLOB;
    v_hls_doc_file_content_rec hls_doc_file_content%ROWTYPE;
  BEGIN
  
    SELECT * INTO v_hls_doc_file_content_rec FROM hls_doc_file_content h WHERE h.content_id = p_content_id;
  
    v_content := get_bookmark_value_common(p_session_id     => p_session_id,
                                           p_document_id    => v_hls_doc_file_content_rec.document_id,
                                           p_document_table => v_hls_doc_file_content_rec.document_table,
                                           p_bookmark       => p_bookmark,
                                           p_user_id        => p_user_id);
    RETURN v_content;
  
  END;
  PROCEDURE ins_hls_doc_file_content
  (
    p_content_id     OUT hls_doc_file_content.content_id%TYPE,
    p_document_id    hls_doc_file_content.document_id%TYPE,
    p_document_table hls_doc_file_content.document_table%TYPE,
    p_clause_usage   hls_doc_file_content.clause_usage%TYPE,
    p_templet_id     hls_doc_file_content.templet_id%TYPE,
    p_user_id hls_doc_file_content.created_by%TYPE,
    p_ref_v01 hls_doc_file_content.ref_v01%TYPE DEFAULT NULL,
    p_ref_v02 hls_doc_file_content.ref_v02%TYPE DEFAULT NULL,
    p_ref_v03 hls_doc_file_content.ref_v03%TYPE DEFAULT NULL,
    p_ref_v04 hls_doc_file_content.ref_v04%TYPE DEFAULT NULL,
    p_ref_v05 hls_doc_file_content.ref_v05%TYPE DEFAULT NULL,
    p_ref_n01 hls_doc_file_content.ref_n01%TYPE DEFAULT NULL,
    p_ref_n02 hls_doc_file_content.ref_n02%TYPE DEFAULT NULL,
    p_ref_n03 hls_doc_file_content.ref_n03%TYPE DEFAULT NULL,
    p_ref_n04 hls_doc_file_content.ref_n04%TYPE DEFAULT NULL,
    p_ref_n05 hls_doc_file_content.ref_n05%TYPE DEFAULT NULL,
    p_ref_d01 hls_doc_file_content.ref_d01%TYPE DEFAULT NULL,
    p_ref_d02 hls_doc_file_content.ref_d02%TYPE DEFAULT NULL,
    p_ref_d03 hls_doc_file_content.ref_d03%TYPE DEFAULT NULL,
    p_ref_d04 hls_doc_file_content.ref_d04%TYPE DEFAULT NULL,
    p_ref_d05 hls_doc_file_content.ref_d05%TYPE DEFAULT NULL
  ) IS
    v_content_number hls_doc_file_content.content_number%TYPE;
    v_templet_id     NUMBER;
    v_content_id     NUMBER;
    v_cashflow_id    NUMBER;
  BEGIN
    BEGIN
      SELECT t.content_id
        INTO v_content_id
        FROM hls_doc_file_content t
       WHERE t.document_table = p_document_table
             AND t.document_id = p_document_id
             AND t.clause_usage = p_clause_usage;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
    IF v_content_id IS NOT NULL THEN
      p_content_id := v_content_id;
      RETURN;
    END IF;
    p_content_id := hls_doc_file_content_s.NEXTVAL;
  
    IF p_document_table = 'CON_CONTRACT_CASHFLOW' AND p_clause_usage = 'PAYMENT_NOTICE' THEN
      SELECT t.contract_number || '第' || ca.times || '期租金通知书'
        INTO v_content_number
        FROM con_contract          t,
             con_contract_cashflow ca
       WHERE ca.contract_id = t.contract_id
             AND ca.cashflow_id = p_document_id;
    
      BEGIN
        SELECT h.templet_id
          INTO v_templet_id
          FROM select_payment_content t,
               con_contract_cashflow  c,
               hls_doc_file_templet   h
         WHERE t.contract_id = c.contract_id
               AND c.cashflow_id = p_document_id
               AND h.templet_code = t.templet_code;
      EXCEPTION   
        WHEN NO_DATA_FOUND THEN--模板未选择，请返回第一期进行模板选择后再行操作
          --SELECT t.templet_id INTO v_templet_id FROM hls_doc_file_templet t WHERE t.templet_code = 'PAYMENT_NOTICE2';
        sys_raise_app_error_pkg.raise_sys_others_error(p_message                 => '模板未选择，请选择该合同任意一期进行模板选择后再行操作！',
                                                       p_created_by              => p_user_id,
                                                       p_package_name            => 'hls_doc_file_templet_pkg',
                                                       p_procedure_function_name => 'delete_hls_doc_file_content');
        raise_application_error(sys_raise_app_error_pkg.c_error_number,
                                sys_raise_app_error_pkg.g_err_line_id);
         WHEN TOO_MANY_ROWS THEN--模板选择多个
        sys_raise_app_error_pkg.raise_sys_others_error(p_message                 => '该合同模板选择多个，请仅选择一个合同模板！',
                                                       p_created_by              => p_user_id,
                                                       p_package_name            => 'hls_doc_file_templet_pkg',
                                                       p_procedure_function_name => 'delete_hls_doc_file_content');
        raise_application_error(sys_raise_app_error_pkg.c_error_number,
                                sys_raise_app_error_pkg.g_err_line_id);
        WHEN OTHERS THEN
        sys_raise_app_error_pkg.raise_sys_others_error(p_message                 => '模板获取异常，请联系管理员！',
                                                       p_created_by              => p_user_id,
                                                       p_package_name            => 'hls_doc_file_templet_pkg',
                                                       p_procedure_function_name => 'delete_hls_doc_file_content');
        raise_application_error(sys_raise_app_error_pkg.c_error_number,
                                sys_raise_app_error_pkg.g_err_line_id);
      END;
    END IF;
  
  
    INSERT INTO hls_doc_file_content
      (content_id,
       document_id,
       document_table,
       clause_usage,
       templet_id,
       creation_date,
       created_by,
       last_update_date,
       last_updated_by,
       ref_v01,
       ref_v02,
       ref_v03,
       ref_v04,
       ref_v05,
       ref_n01,
       ref_n02,
       ref_n03,
       ref_n04,
       ref_n05,
       ref_d01,
       ref_d02,
       ref_d03,
       ref_d04,
       ref_d05,
       content_number)
    VALUES
      (p_content_id,
       p_document_id,
       p_document_table,
       p_clause_usage,
       v_templet_id,
       SYSDATE,
       p_user_id,
       SYSDATE,
       p_user_id,
       p_ref_v01,
       p_ref_v02,
       p_ref_v03,
       p_ref_v04,
       p_ref_v05,
       p_ref_n01,
       p_ref_n02,
       p_ref_n03,
       p_ref_n04,
       p_ref_n05,
       p_ref_d01,
       p_ref_d02,
       p_ref_d03,
       p_ref_d04,
       p_ref_d05,
       v_content_number);
  END;
  
  /*删除已生成的租金通知书 hongquan.dai 20180829*/
  PROCEDURE delete_hls_doc_file_content
  (p_user_id     NUMBER,
   p_cashflow_id NUMBER
  ) IS
  v_content_id NUMBER;
  BEGIN
    --判断是否为存在且为操作用户生成
    SELECT t.content_id 
      INTO v_content_id
      FROM  hls_doc_file_content t  
    WHERE t.document_id = p_cashflow_id;
      
   --删除  
    DELETE FROM hls_doc_file_content t  
    WHERE t.document_id = p_cashflow_id;
  --更新为未打印   
   UPDATE con_contract_cashflow t
   SET PRINT_NOTICE_FLAG = 'N'
    WHERE t.cashflow_id = p_cashflow_id;
    
  --插入日志表
  INSERT INTO delete_hls_doc_file_logs(user_id,
                                       delete_date,
                                       cashflow_id,
                                       content_id) 
                                VALUES(p_user_id,
                                       SYSDATE,
                                       p_cashflow_id,
                                       v_content_id);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        sys_raise_app_error_pkg.raise_sys_others_error(p_message                 => '未生成租金通知书或此租金通知书不是由您生成，不能删除！',
                                                       p_created_by              => p_user_id,
                                                       p_package_name            => 'hls_doc_file_templet_pkg',
                                                       p_procedure_function_name => 'delete_hls_doc_file_content');
        raise_application_error(sys_raise_app_error_pkg.c_error_number,
                                sys_raise_app_error_pkg.g_err_line_id);
      WHEN OTHERS THEN
        sys_raise_app_error_pkg.raise_sys_others_error(p_message                 => '删除异常，请联系管理员！',
                                                       p_created_by              => p_user_id,
                                                       p_package_name            => 'hls_doc_file_templet_pkg',
                                                       p_procedure_function_name => 'delete_hls_doc_file_content');
        raise_application_error(sys_raise_app_error_pkg.c_error_number,
                                sys_raise_app_error_pkg.g_err_line_id);
    END;

END hls_doc_file_templet_pkg;
/
