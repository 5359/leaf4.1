-- Create table
create table HLS_DOC_FILE_TMP_PARA_LINK
(
  LINK_ID          NUMBER not null,
  TEMPLET_PARA_ID  NUMBER not null,
  TEMPLET_ID       NUMBER not null,
  ENABLED_FLAG     VARCHAR2(1),
  CREATION_DATE    DATE not null,
  CREATED_BY       NUMBER not null,
  LAST_UPDATE_DATE DATE not null,
  LAST_UPDATED_BY  NUMBER not null,
  FONT_FAMILY      VARCHAR2(30),
  FONT_SIZE        NUMBER,
  UNDERLINE        VARCHAR2(30)
);
-- Add comments to the columns
comment on column HLS_DOC_FILE_TMP_PARA_LINK.LINK_ID
  is ' 链接ID';
comment on column HLS_DOC_FILE_TMP_PARA_LINK.TEMPLET_PARA_ID
  is '模板参数ID';
comment on column HLS_DOC_FILE_TMP_PARA_LINK.TEMPLET_ID
  is '模板ID';
-- Create/Recreate primary, unique and foreign key constraints
alter table HLS_DOC_FILE_TMP_PARA_LINK
  add constraint HLS_DOC_FILE_TMP_PARA_LINK_PK primary key (LINK_ID)
  using index ;
-- Create/Recreate indexes
create index HLS_DOC_FILE_TMP_PARA_LINK_N1 on HLS_DOC_FILE_TMP_PARA_LINK (TEMPLET_ID);
