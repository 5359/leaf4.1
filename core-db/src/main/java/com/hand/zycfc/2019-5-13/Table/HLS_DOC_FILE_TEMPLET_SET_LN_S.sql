-- Create sequence
create sequence HLS_DOC_FILE_TEMPLET_SET_LN_S
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;
