-- Create table
create table HLS_DOC_FILE_PARA_TABLE
(
  PARA_TABLE_ID    NUMBER not null,
  TEMPLET_PARA_ID  NUMBER,
  COLUMN_NAME      VARCHAR2(200),
  COLUMN_DESC      VARCHAR2(200),
  COLUMN_NUM       NUMBER,
  ENABLED_FLAG     VARCHAR2(1),
  CREATION_DATE    DATE,
  CREATED_BY       NUMBER,
  LAST_UPDATE_DATE DATE,
  LAST_UPDATED_BY  NUMBER,
  ALIGNMENT        VARCHAR2(200),
  COLUMN_WIDTH     NUMBER
);
-- Create/Recreate primary, unique and foreign key constraints
alter table HLS_DOC_FILE_PARA_TABLE
  add constraint HLS_DOC_FILE_PARA_TABLE_PK primary key (PARA_TABLE_ID)
  using index ;
