begin

  sys_code_pkg.insert_sys_code('BOOKMARK_TYPE', 'doc书签类型', 'doc书签类型', 'doc书签类型', 'ZHS', '');
  sys_code_pkg.update_sys_code('BOOKMARK_TYPE', 'doc书签类型', 'doc书签类型', 'doc书签类型', 'US', '');

  sys_code_pkg.insert_sys_code_value('BOOKMARK_TYPE','FORM_TABLE','横向表单','ZHS','');
  sys_code_pkg.update_sys_code_value('BOOKMARK_TYPE','FORM_TABLE','横向表单','US','');

  sys_code_pkg.insert_sys_code_value('BOOKMARK_TYPE','TABLE','表单','ZHS','');
  sys_code_pkg.update_sys_code_value('BOOKMARK_TYPE','TABLE','表单','US','');

  sys_code_pkg.insert_sys_code_value('BOOKMARK_TYPE','TEXTAREA','文本域','ZHS','');
  sys_code_pkg.update_sys_code_value('BOOKMARK_TYPE','TEXTAREA','文本域','US','');

  sys_code_pkg.insert_sys_code_value('BOOKMARK_TYPE','TEXT','文本','ZHS','');
  sys_code_pkg.update_sys_code_value('BOOKMARK_TYPE','TEXT','文本','US','');

  sys_code_pkg.insert_sys_code_value('BOOKMARK_TYPE','TABLE_AREA','表单域','ZHS','');
  sys_code_pkg.update_sys_code_value('BOOKMARK_TYPE','TABLE_AREA','表单域','US','');
  
end;