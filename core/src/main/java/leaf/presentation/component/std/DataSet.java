package leaf.presentation.component.std;

import com.hand.hap.cache.Cache;
import com.hand.hap.cache.CacheManager;
import com.hand.hap.core.BaseConstants;
import leaf.annotation.LovField;
import leaf.application.ApplicationViewConfig;
import leaf.application.features.ILookupCodeProvider;
import leaf.bm.BusinessModel;
import leaf.bm.Field;
import leaf.bm.IModelFactory;
import leaf.presentation.BuildSession;
import leaf.presentation.ViewContext;
import leaf.presentation.component.std.config.ComponentConfig;
import leaf.presentation.component.std.config.DataSetConfig;
import leaf.presentation.component.std.config.DataSetFieldConfig;
import leaf.utils.ConfigUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import uncertain.composite.CompositeMap;
import uncertain.ocm.IObjectRegistry;

import java.io.IOException;
import java.util.*;

/**
 * @author <a href="mailto:njq.niu@hand-china.com">vincent</a>
 * @version $Id: DataSet.java,v 1.1 2016/09/02 08:21:30 wuhuazhen2689 Exp $
 */
@SuppressWarnings("unchecked")
public class DataSet extends Component {

    public static final String VERSION = "$Revision: 1.1 $";

    private static final String VALID_SCRIPT = "validscript";
    private IModelFactory mFactory;
    private ILookupCodeProvider lookupProvider;
    private IObjectRegistry iObjectRegistry;

    public DataSet(IObjectRegistry registry, IModelFactory factory,
                   ILookupCodeProvider lookupProvider) {
        super(registry);
        this.iObjectRegistry = registry;
        this.mFactory = factory;
        this.lookupProvider = lookupProvider;
    }

    public DataSet(IObjectRegistry registry, IModelFactory factory) {
        super(registry);
        this.mFactory = factory;
    }

    // public DataSet(IObjectRegistry registry,IModelFactory factory){
    // this.mRegistry = registry;
    // this.mFactory = factory;
    // mApplicationConfig = (ApplicationConfig)
    // mRegistry.getInstanceOfType(IApplicationConfig.class);
    // }

    private void initLovService(String baseModel, BuildSession session,
                                CompositeMap field) throws IOException {
        BusinessModel bm = null;
        bm = mFactory.getModelForRead(baseModel.split("\\?")[0]);
        Field[] bmfields = bm.getFields();
        JSONArray lovDisplayFields = new JSONArray();
        if (null != bmfields) {
            for (int i = 0, l = bmfields.length; i < l; i++) {
                Field f = bmfields[i];
                if (f.isForDisplay()) {
                    DataSetFieldConfig dfc = DataSetFieldConfig.getInstance(f
                            .getObjectContext());
                    dfc.setPrompt(session.getLocalizedPrompt(dfc.getPrompt()));
                    lovDisplayFields
                            .put(new JSONObject(dfc.getObjectContext()));
                }
            }
        }
        field.put("displayFields", lovDisplayFields);
    }

//    private List<CodeValue> getEnabledCodeValues(Code code) {
//        List<CodeValue> enabledCodeValues = new ArrayList<>();
//        List<CodeValue> allCodeValues = code.getCodeValues();
//        if (allCodeValues != null) {
//            for (CodeValue codevalue : allCodeValues) {
//                if (BaseConstants.YES.equals(codevalue.getEnabledFlag())) {
//                    enabledCodeValues.add(codevalue);
//                }
//            }
//        }
//        return enabledCodeValues;
//    }

    public void onCreateViewContent(BuildSession session, ViewContext context)
            throws IOException {
        super.onCreateViewContent(session, context);
        CompositeMap view = context.getView();
        CompositeMap model = context.getModel();
        Map map = context.getMap();
        JSONArray fieldList = new JSONArray();
        DataSetConfig dsc = DataSetConfig.getInstance(view);
        boolean mDefaultFuzzyFetch = ApplicationViewConfig.DEFAULT_FUZZY_FETCH;
        boolean mDefaultModifiedCheck = ApplicationViewConfig.DEFAULT_MODIFIED_CHECK;
        boolean mDefaultAutoCount = ApplicationViewConfig.DEFAULT_AUTO_COUNT;
        int mDefaultPageSize = ApplicationViewConfig.DEFAULT_PAGE_SIZE;
        boolean mDefaultInfiniteLoad = ApplicationViewConfig.DEFAULT_INFINITE_LOAD;
        if (mApplicationConfig != null) {
            ApplicationViewConfig view_config = mApplicationConfig.getApplicationViewConfig();
            if (null != view_config) {
                mDefaultFuzzyFetch = view_config.getDefaultFuzzyFetch();
                mDefaultModifiedCheck = view_config.getDefaultModifiedCheck();
                mDefaultAutoCount = view_config.getDefaultAutoCount();
                mDefaultPageSize = view_config.getDefaultPageSize();
                mDefaultInfiniteLoad = view_config.getDefaultInfiniteLoad();
            }
        }
        CompositeMap fields = dsc.getFields();
        if (fields != null) {
            Iterator it = fields.getChildIterator();
            if (it != null)
                while (it.hasNext()) {
                    CompositeMap field = (CompositeMap) it.next();
                    DataSetFieldConfig sdfc = DataSetFieldConfig
                            .getInstance(field);

                    if (null != field
                            .getString(DataSetFieldConfig.PROPERTITY_REQUIRED))
                        field.putBoolean(
                                DataSetFieldConfig.PROPERTITY_REQUIRED,
                                sdfc.getRequired(model));
                    if (null != field
                            .getString(DataSetFieldConfig.PROPERTITY_READONLY)) {
                        field.putBoolean(
                                DataSetFieldConfig.PROPERTITY_READONLY,
                                sdfc.getReadOnly(model));
                    }
                    if (null != field
                            .getString(DataSetFieldConfig.PROPERTITY_EDITABLE))
                        field.putBoolean(
                                DataSetFieldConfig.PROPERTITY_EDITABLE,
                                sdfc.getEditable(model));
                    // if(null!=field.getString(DataSetFieldConfig.PROPERTITY_TOOLTIP))
                    // field.putString(DataSetFieldConfig.PROPERTITY_TOOLTIP,
                    // sdfc.getTooltip());
                    if (null != field
                            .getString(DataSetFieldConfig.PROPERTITY_AUTO_COMPLETE))
                        field.putBoolean(
                                DataSetFieldConfig.PROPERTITY_AUTO_COMPLETE,
                                sdfc.getAutoComplete());
                    boolean fuzzyFetch = sdfc.getFuzzyFetch(mDefaultFuzzyFetch);
                    if (fuzzyFetch)
                        field.putBoolean(
                                DataSetFieldConfig.PROPERTITY_FUZZY_FETCH,
                                fuzzyFetch);
                    if (sdfc.getDefaultValue() != null)
                        field.putString(
                                DataSetFieldConfig.PROPERTITY_DEFAULTVALUE,
                                session.parseString(sdfc.getDefaultValue(),
                                        model));

                    String options = field
                            .getString(DataSetFieldConfig.PROPERTITY_OPTIONS);
                    if (options != null) {
                        field.putString(DataSetFieldConfig.PROPERTITY_OPTIONS,
                                uncertain.composite.TextParser.parse(options,
                                        model));
                    }

                    if (null != field
                            .getString(DataSetFieldConfig.PROPERTITY_FETCH_REMOTE))
                        field.putBoolean(
                                DataSetFieldConfig.PROPERTITY_FETCH_REMOTE,
                                sdfc.getFetchRemote());
                    if (null != field
                            .getString(DataSetFieldConfig.PROPERTITY_FETCH_RECORD))
                        field.putBoolean(
                                DataSetFieldConfig.PROPERTITY_FETCH_RECORD,
                                sdfc.getFetchRecord());
                    if (null != field
                            .getString(DataSetFieldConfig.PROPERTITY_FETCH_SINGLE))
                        field.putBoolean(
                                DataSetFieldConfig.PROPERTITY_FETCH_SINGLE,
                                sdfc.getFetchSingle());

                    String lovService = field
                            .getString(DataSetFieldConfig.PROPERTITY_LOV_SERVICE);
//					add controller support
                    if (lovService != null && lovService.startsWith("/")) {
//				        lovService contains of controllerUrl|dto
                        String[] split = StringUtils.split(lovService, "~");
                        // split.lenth should be 2
                        if (split.length != 2) {
                            throw new RuntimeException("lovService is not configured properly");
                        }
                        String controller = split[0];
                        String dto = split[1];
                        initLovService(controller, dto, session, field);
                    } else if (lovService != null && lovService.length() > 0) {
                        String baseModel = uncertain.composite.TextParser
                                .parse(lovService, model);
                        field.putString(
                                DataSetFieldConfig.PROPERTITY_LOV_SERVICE,
                                baseModel);
                        initLovService(baseModel, session, field);
                    }
                    String lovModel = field
                            .getString(DataSetFieldConfig.PROPERTITY_LOV_MODEL);
                    if (lovModel != null) {
                        String baseModel = uncertain.composite.TextParser
                                .parse(lovModel, model);
                        field.putString(
                                DataSetFieldConfig.PROPERTITY_LOV_MODEL,
                                baseModel);
                        initLovService(baseModel, session, field);
                    }
                    String lovUrl = field
                            .getString(DataSetFieldConfig.PROPERTITY_LOV_URL);
                    if (lovUrl != null) {
                        field.putString(DataSetFieldConfig.PROPERTITY_LOV_URL,
                                uncertain.composite.TextParser.parse(lovUrl,
                                        model));
                    }
                    String lovTitle = field
                            .getString(DataSetFieldConfig.PROPERTITY_TITLE);
                    if (lovTitle != null) {
                        field.putString(DataSetFieldConfig.PROPERTITY_TITLE,
                                session.getLocalizedPrompt(lovTitle));
                    }
                    String requiredMessage = field
                            .getString(DataSetFieldConfig.PROPERTITY_REQUIRED_MESSAGE);
                    if (requiredMessage != null) {
                        field.putString(
                                DataSetFieldConfig.PROPERTITY_REQUIRED_MESSAGE,
                                session.parseString(requiredMessage, model));
                    }else{
                        if(field.getBoolean(DataSetFieldConfig.PROPERTITY_REQUIRED)!=null&&field.getBoolean(DataSetFieldConfig.PROPERTITY_REQUIRED)&&field.getString(DataSetFieldConfig.PROPERTITY_PROMPT)!=null){
                            String prompt =session.getLocalizedPrompt(field.getString(DataSetFieldConfig.PROPERTITY_PROMPT));
                            field.putString(
                                    DataSetFieldConfig.PROPERTITY_REQUIRED_MESSAGE,
                                    session.parseString(prompt+"${l:HLS.NOT_NULL}",model));
                        }
                    }
                    String returnField = sdfc.getReturnField();// field.getString(DataSetFieldConfig.PROPERTITY_RETURN_FIELD,
                    // "");
                    boolean addReturn = returnField != null;// !"".equals(returnField);

                    // 删除不必要的信息
                    field.remove("databasetype");
                    String datatype = field.getString("datatype");
                    if ("java.lang.String".equals(datatype))
                        field.remove("datatype");

                    JSONObject json = new JSONObject(field);
                    CompositeMap mapping = sdfc.getMapping();// field.getChild(DataSetConfig.PROPERTITY_MAPPING);
                    List maplist = new ArrayList();
                    if (mapping != null) {
                        Iterator mit = mapping.getChildIterator();
                        while (mit.hasNext()) {
                            CompositeMap mapfield = (CompositeMap) mit.next();
                            if (returnField != null
                                    && returnField.equals(mapfield
                                    .getString("to"))) {
                                addReturn = false;
                            }
                            JSONObject mj = new JSONObject(mapfield);
                            maplist.add(mj);
                        }
                    }
                    if (addReturn) {
                        CompositeMap returnmap = new CompositeMap("map");
                        returnmap.putString("from", sdfc.getValueField());
                        returnmap.putString("to", returnField);
                        JSONObject jo = new JSONObject(returnmap);
                        maplist.add(jo);
                    }
                    if (maplist.size() > 0) {
                        try {
                            json.put(DataSetConfig.PROPERTITY_MAPPING, maplist);
                        } catch (JSONException e) {
                            throw new IOException(e);
                        }
                    }
                    fieldList.put(json);
                }
        }

        StringBuilder sb = new StringBuilder();
        String attachTab = dsc.getValidListener();
        if (attachTab != null) {
            String[] ts = attachTab.split(",");
            for (int i = 0; i < ts.length; i++) {
                String tid = ts[i];
                sb.append("$('"
                        + map.get(ComponentConfig.PROPERTITY_ID)
                        + "').on('valid',function(ds, record, name, valid){if(!valid && !Ext.get('"
                        + tid + "').hasActiveFx()) Ext.get('" + tid
                        + "').frame('ff0000', 3, { duration: 1 })});\n");
            }
        }
        map.put(VALID_SCRIPT, sb.toString());

        CompositeMap datas = dsc.getDatas();
        JSONArray dataList = new JSONArray();
        List list = null;
        Set dataHead = new HashSet();
        if (datas != null) {
            String ds = datas
                    .getString(DataSetConfig.PROPERTITY_DATASOURCE, "");
            if (ds.equals("")) {
                list = datas.getChilds();
                Iterator dit = list.iterator();
                while (dit.hasNext()) {
                    CompositeMap item = (CompositeMap) dit.next();
                    Iterator it = item.keySet().iterator();
                    dataHead.addAll(item.keySet());
                    while (it.hasNext()) {
                        String key = (String) it.next();
                        Object valueKey = item.get(key);
                        if (valueKey != null) {
                            String value = uncertain.composite.TextParser
                                    .parse(valueKey.toString(), model);
                            if (value.equals(valueKey.toString())) {
                                item.put(key, valueKey);
                            } else {
                                item.put(key, value);
                            }
                        }
                    }
                }
            } else {
                CompositeMap data = (CompositeMap) model.getObject(ds);
                if (data != null) {
                    list = data.getChilds();
                }
            }
        }
        String lcode = dsc.getLookupCode();
        if (lcode != null) {
            list = new ArrayList();
            if (ConfigUtils.isLeafTargetVersion(ConfigUtils.VERSION_1_0)) {
                ILookupCodeProvider provider = this.lookupProvider;
                if (provider != null) {
                    try {
                        list = provider.getLookupList(session.getLanguage(), lcode);
                    } catch (Exception e) {
                        throw new IOException(e);
                    }
                    // if(llist!=null){
                    // Iterator it = llist.iterator();
                    // while(it.hasNext()){
                    // JSONObject json = new JSONObject((CompositeMap)it.next());
                    // dataList.put(json);
                    // }
                    // }
                }
            } else {
//                Object springApplicationContext = this.iObjectRegistry.getInstanceOfType(ApplicationContext.class);
//                if (springApplicationContext == null) {
//                    throw new RuntimeException("could not get applicationContext");
//                }
//                Map<String, CacheManager> beans = ((ApplicationContext) springApplicationContext).getBeansOfType(CacheManager.class);
//                if (beans != null) {
//                    CacheManager cacheManager = null;
//                    for (String s : beans.keySet()) {
//                        cacheManager = beans.get(s);
//                        SysCodeCache codeCache = (SysCodeCache) (Cache) cacheManager.getCache(BaseConstants.CACHE_CODE);
//                        String local = null;
//                        Code code2 = codeCache.getValue(lcode + "." + ConfigUtils.getLocal(session.getLanguage()));
//                        if (code2 != null && !BaseConstants.NO.equals(code2.getEnabledFlag())) {
//                            List<CodeValue> enabledCodeValues = getEnabledCodeValues(code2);
//                            for (int i = 0; i < enabledCodeValues.size(); i++) {
//                                CodeValue codeValue = enabledCodeValues.get(i);
//                                CompositeMap codeValueMap = new CompositeMap();
//                                codeValueMap.put("code_value", codeValue.getValue());
//                                codeValueMap.put("code_value_name", codeValue.getMeaning() == null ? codeValue.getDescription() : codeValue.getMeaning());
//                                list.add(codeValueMap);
//
//                            }
//                        }
//                    }
//                }

            }
        }
        if (list != null && !list.isEmpty()) {
            Iterator lit = list.iterator();
            while (lit.hasNext()) {
                dataHead.addAll(((CompositeMap) lit.next()).keySet());
            }

            addConfig(DataSetConfig.PROPERTITY_DATA_HEAD, new JSONArray(
                    dataHead));
            Iterator dit = list.iterator();
            while (dit.hasNext()) {
                CompositeMap item = (CompositeMap) dit.next();
                JSONArray json = new JSONArray();
                Iterator it = dataHead.iterator();
                while (it.hasNext()) {
                    json.put(item.get(it.next()));
                }
                dataList.put(json);
            }
        }
        if (fieldList.length() != 0)
            addConfig(DataSetConfig.PROPERTITY_FIELDS, fieldList);
        if (dataList.length() != 0)
            addConfig(DataSetConfig.PROPERTITY_DATAS, dataList);
        if (!"".equals(dsc.getQueryDataSet())) {
            String queryDataset = uncertain.composite.TextParser.parse(
                    dsc.getQueryDataSet(), model);
            addConfig(DataSetConfig.PROPERTITY_QUERYDATASET, queryDataset);
        }
        if (!"".equals(dsc.getQueryUrl(model))) {
            String queryUrl = uncertain.composite.TextParser.parse(
                    dsc.getQueryUrl(), model);
            addConfig(DataSetConfig.PROPERTITY_QUERYURL, queryUrl);
        }
        if (!"".equals(dsc.getQueryMethod(model))) {
            String queryMethod = uncertain.composite.TextParser.parse(
                    dsc.getQueryMethod(), model);
            addConfig(DataSetConfig.PROPERTITY_QUERY_METHOD, queryMethod);
        }
        if (!"".equals(dsc.getDeleteMethod(model))) {
            String deleteMethod = uncertain.composite.TextParser.parse(
                    dsc.getDeleteMethod(), model);
            addConfig(DataSetConfig.PROPERTITY_DELETE_METHOD, deleteMethod);
        }
        if (!"".equals(dsc.getDeleteUrl(model))) {
            String deleteUrl = uncertain.composite.TextParser.parse(
                    dsc.getDeleteUrl(), model);
            addConfig(DataSetConfig.PROPERTITY_DELETEURL, deleteUrl);
        }
        if (!"".equals(dsc.getSubmitUrl())) {
            String submitUrl = uncertain.composite.TextParser.parse(
                    dsc.getSubmitUrl(), model);
            addConfig(DataSetConfig.PROPERTITY_SUBMITURL, submitUrl);
        }
        // Hybris
        if (!"".equals(dsc.getRestDataFormat())) {
            String restDataFormat = uncertain.composite.TextParser.parse(
                    dsc.getRestDataFormat(), model);
            addConfig(DataSetConfig.PROPERTITY_REST_DATA_FORMAT, restDataFormat);
        }
        if (!"".equals(dsc.getQueryDataFormat())) {
            String queryDataFormat = uncertain.composite.TextParser.parse(
                    dsc.getQueryDataFormat(), model);
            addConfig(DataSetConfig.PROPERTITY_QUERY_DATA_FORMAT, queryDataFormat);
        }
        if (dsc.isHybrisWS()) {
            addConfig(DataSetConfig.PROPERTITY_HYBRIS_WS, true);
        }

        if (!"".equals(dsc.getBindTarget()))
            addConfig(DataSetConfig.PROPERTITY_BINDTARGET,
                    uncertain.composite.TextParser.parse(dsc.getBindTarget(),
                            model));
        if (!"".equals(dsc.getBindName()))
            addConfig(DataSetConfig.PROPERTITY_BINDNAME,
                    uncertain.composite.TextParser.parse(dsc.getBindName(),
                            model));
        if (dsc.isFetchAll(model))
            addConfig(DataSetConfig.PROPERTITY_FETCHALL,
                    Boolean.valueOf(dsc.isFetchAll(model)));
        ;
        boolean isAutoQuery = dsc.isAutoQuery(model);
        if (isAutoQuery)
            addConfig(DataSetConfig.PROPERTITY_AUTO_QUERY, Boolean.valueOf(isAutoQuery));
        if (dsc.isAutoPageSize(model))
            addConfig(DataSetConfig.PROPERTITY_AUTO_PAGE_SIZE,
                    Boolean.valueOf(dsc.isAutoPageSize(model)));
        addConfig(DataSetConfig.PROPERTITY_PAGEID, session.getSessionContext()
                .getString("pageid", ""));
        addConfig(DataSetConfig.PROPERTITY_TOTALCOUNT_FIELD,
                dsc.getTotalCountField());
        addConfig(DataSetConfig.PROPERTITY_MODIFIED_CHECK,
                dsc.isModifiedCheck(mDefaultModifiedCheck));
        addConfig(DataSetConfig.PROPERTITY_INFINITE_LOAD,
                dsc.isInfiniteLoad(mDefaultInfiniteLoad));
        BusinessModel bm = null;
        Integer mps = null;
        String md = dsc.getModel();
        if (md != null)
            bm = mFactory.getModelForRead(uncertain.composite.TextParser.parse(
                    md, model));
        if (bm != null) {
            mps = bm.getMaxPageSize();
        }
        if (mps != null) {
            addConfig(DataSetConfig.PROPERTITY_MAX_PAGESIZE, new Integer(mps));
        } else {
            addConfig(DataSetConfig.PROPERTITY_MAX_PAGESIZE,
                    new Integer(dsc.getMaxPageSize()));
        }
        addConfig(DataSetConfig.PROPERTITY_PAGESIZE,
                dsc.getPageSize(model, mDefaultPageSize));

        addConfig(DataSetConfig.PROPERTITY_AUTO_COUNT,
                Boolean.valueOf(dsc.isAutoCount(model, mDefaultAutoCount)));
        if (dsc.getSortType() != null)
            addConfig(DataSetConfig.PROPERTITY_SORT_TYPE, dsc.getSortType());
        if (dsc.getNotification() != null)
            addConfig(DataSetConfig.PROPERTITY_NOTIFICATION,
                    dsc.getNotification());

        if (dsc.isAutoCreate())
            addConfig(DataSetConfig.PROPERTITY_AUTO_CREATE,
                    Boolean.valueOf(dsc.isAutoCreate()));
        if (dsc.isSelectable())
            addConfig(DataSetConfig.PROPERTITY_SELECTABLE,
                    Boolean.valueOf(dsc.isSelectable()));
        if (null != dsc.getSelectFunction())
            addConfig(DataSetConfig.PROPERTITY_SELECT_FUNCTION,
                    dsc.getSelectFunction());
        if (!DataSetConfig.DEFAULT_SELECTION_MODEL.equals(dsc
                .getSelectionModel()))
            addConfig(DataSetConfig.PROPERTITY_SELECTION_MODEL,
                    dsc.getSelectionModel());
        String pf = uncertain.composite.TextParser.parse(
                dsc.getProcessFunction(), model);
        if (!"".equals(pf)) {
            addConfig(DataSetConfig.PROPERTITY_PROCESS_FUNCTION, pf);

        }
        map.put(CONFIG, getConfigString());
    }

    private void initLovService(String controller, String dto, BuildSession session, CompositeMap field) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName(dto);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        java.lang.reflect.Field[] fields = clazz.getFields();
        JSONArray lovDisplayFields = new JSONArray();
        for (java.lang.reflect.Field f : fields) {
            boolean annotationPresent = f.isAnnotationPresent(LovField.class);
            if (!annotationPresent) {
                continue;
            }
            LovField annotation = f.getAnnotation(LovField.class);
            boolean forDisplay = annotation.forDisplay();
            if (forDisplay) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("prompt", session.getLocalizedPrompt(annotation.prompt()));
                jsonObject.put("displaywith", annotation.displayWidth());
                jsonObject.put("queryWidth", annotation.queryWidth());
                jsonObject.put("databasetype", annotation.databaseType());
                jsonObject.put("forquery", annotation.forQuery());
                jsonObject.put("name", f.getName());
                jsonObject.put("datatype", f.getType().getName());
                lovDisplayFields.put(jsonObject);
            }
        }
        field.put("displayFields", lovDisplayFields);
        field.put("controller", controller);
        field.put("dto", dto);
    }
}
